# Excel Dashboard Project
<img src = "pic.png" alt="exel" style="width:100%;">


<img src = "pic2.png" alt="exel" style="width:100%;">

<br /><br />

This interactive dashboard is created by Excel 2016. I went through this [**tutorial**](https://www.youtube.com/watch?v=cKkXtyjleX4&t=2s) 
## Maintainer

- [**Mahdieh Sajedipoor**](https://gitlab.com/Mahdieh_sjp)


## Motivation
I wanted to get to know Excel and what it is capable of.<br />
I have to mention that I didn't just copy what was in tutorial, I learned a lot from it.

